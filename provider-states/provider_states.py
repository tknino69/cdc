#!/usr/local/bin/python3

from flask import Flask, jsonify, request

app = Flask(__name__)


@app.route('/pact/provider_states', methods=['POST'])
def provider_states():
    mapping = {
        'provider in some state': some_provider_state,
    }
    mapping[request.json['state']]()
    return jsonify({'result': request.json['state']})


def some_provider_state():
    """
    There should be some decent stuff here; may be something like db CRUD operation
    :return:
    """
    pass


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
