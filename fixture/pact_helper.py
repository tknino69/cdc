import os
import pytest
import requests
from pactman import Provider, Consumer
from datetime import datetime as dt
from datetime import timedelta as td

TIMEOUT = 3


@pytest.fixture
def upload_pact_to_broker():
    def _upload(broker, provider, consumer, consumer_version):
        start_time = dt.now()
        end_time = dt.now()
        while end_time - start_time <= td(seconds=TIMEOUT):
            try:
                if os.path.exists('{}-{}-pact.json'.format(consumer, provider)):
                    break
                end_time = dt.now()
            except:
                pass
        p = requests.put('http://{}/pacts/provider/{}/consumer/{}/version/{}'.format(broker, provider, consumer, consumer_version),
                         data=open('{}-{}-pact.json'.format(consumer, provider), 'rb'),
                         headers={'Content-Type': 'application/json'}, verify=False)
        assert p.status_code in [200, 201]
    return _upload


@pytest.fixture
def pact(upload_pact_to_broker):
    provider = os.getenv("PROVIDER")
    consumer = os.getenv("CONSUMER")
    consumer_version = os.getenv("CONSUMER_VERSION")
    broker = 'pactbroker'
    pact = Consumer(consumer).has_pact_with(Provider(provider), version='3.0.0')
    pact.start_service()
    yield pact
    pact.stop_service()
    upload_pact_to_broker(broker, provider, consumer, consumer_version)
