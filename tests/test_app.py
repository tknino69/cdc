import pytest
from pactman import Like
from model.client import Client


@pytest.fixture()
def consumer(pact):
    return Client(pact.uri)


def test_app(pact, consumer):
    expected = '123456789'
    (pact
     .given('provider in some state')
     .upon_receiving("request to get user's phone number")
     .with_request(
        method='GET',
        path=f'/phone/john',
        )
     .will_respond_with(200, body=Like(expected))

     .given('provider in some state')
     .upon_receiving("request to get non-existent user's phone number")
     .with_request(
        method='GET',
        path=f'/phone/micky'
        )
     .will_respond_with(403)
     )
    with pact:
        consumer.get_users_phone(user='john', host=pact.uri)
        consumer.get_users_phone(user='micky', host=pact.uri)
