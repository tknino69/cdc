import requests


class Client:

    def __init__(self, host):
        self.host = host

    @staticmethod
    def get_users_phone(user, host=None):
        return requests.get(f'{host}/phone/{user}')
