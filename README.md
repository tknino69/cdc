# Case Study: GitLab CI and Consumer Driven Contracts


### Objectives:
* Brief introduction to Consumer Driven Contract testing (CDC)
* Demonstrate CI pipeline for CDC testing


#### CDC testing

This is a rather quick 'how-to' contract testing from QA test automation perspective as oppose to complete guide to CDC. There 
are plenty of materials that would give you more complete picture of consumer driven contacts. 
The focus of the exercise is on practical implementation. The source code is [here](https://gitlab.com/tknino69/cdc)

To move forward we need to familiarize ourselves with a few concepts of the workflow:
* CDC tests locate on a Service/Integration level above Unit Tests in Mike Cohn's [Test Automation Pyramid](https://martinfowler.com/articles/practical-test-pyramid.html)
* Contract testing can be applied when there are 2 (or more) services that interact with each other
* This is a consumer driven approach meaning that the first step in implementation is to write test on a consumer side.
The result of the test is a pact (contract) in json format which describes interactions between consumer(eg. web/mobile
 front-end: service that wants to receive some data) and provider (eg. server's API: service that provides data)
* Next step is to verify the pact against the provider. This is entirely driven by [Pact](https://docs.pact.io) framework.

So, let's start with [Consumer testing](https://docs.pact.io/how_pact_works#consumer-testing).
I've used [Pactman](https://github.com/reecetech/pactman) (python version of Pact). 

```
import pytest
from pactman import Like
from model.client import Client


@pytest.fixture()
def consumer(pact):          
    return Client(pact.uri)


def test_app(pact, consumer):
    expected = '123456789'
    (pact
     .given('provider in some state')
     .upon_receiving("request to get user's phone number")
     .with_request(
        method='GET',
        path=f'/phone/john',
        )
     .will_respond_with(200, body=Like(expected))

     .given('provider in some state')
     .upon_receiving("request to get non-existent user's phone number")
     .with_request(
        method='GET',
        path=f'/phone/micky'
        )
        .will_respond_with(404)
     )
    with pact:
        consumer.get_users_phone(user='john', host=pact.uri)
        consumer.get_users_phone(user='micky', host=pact.uri)
```
Using Pact DSL we describe request/response interactions. Once we've run the test 
we get a new file (consumer-provider-pact.json) generated:
```
{
  "consumer": {
    "name": 'basic_client'
  },
  "provider": {
    "name": 'basic_flask_app'
  },
  "interactions": [
    {
      "providerStates": [
        {
          "name": "provider in some state",
          "params": {}
        }
      ],
      "description": "request to get user's phone number",
      "request": {
        "method": "GET",
        "path": "/phone/john"
      },
      "response": {
        "status": 200,
        "body": "123456789",
        "matchingRules": {
          "body": {
            "$": {
              "matchers": [
                {
                  "match": "type"
                }
              ]
            }
          }
        }
      }
    },
    {
      "providerStates": [
        {
          "name": "provider in some state",
          "params": {}
        }
      ],
      "description": "request to get non-existent user's phone number",
      "request": {
        "method": "GET",
        "path": "/phone/micky"
      },
      "response": {
        "status": 404
      }
    }
  ],
  "metadata": {
    "pactSpecification": {
      "version": "3.0.0"
    }
  }
}

```

Next, we need to pass the pact to the provider for verification. This is done via [Pact Broker](https://github.com/pact-foundation/pact_broker).

Pact Broker is a contracts storage with some additional features that allow us to trace services' versions compatibility
and display services network diagram. 

##### Pact Broker

![pact-broker](static/cdc_pact_bro.PNG)


##### Pact

![pact](static/pact.PNG)

##### Matrix

![matrix](static/pact-matrix.png)



[Provider verification](https://docs.pact.io/how_pact_works#provider-verification)

This part of the exercise is done entirely by Pact framework. Once verified the results are sent back to
Pact Broker.
```
provider-verifier_1  | Verifying a pact between basic_client and basic_flask_app
provider-verifier_1  |   Given provider in some state
provider-verifier_1  |     request to get user's phone number
provider-verifier_1  |       with GET /phone/john
provider-verifier_1  |         returns a response which
provider-verifier_1  | WARN: Skipping set up for provider state 'provider in some state' for consumer 'basic_client' as there is no --provider-states-setup-url specified.
provider-verifier_1  |           has status code 200
provider-verifier_1  |           has a matching body
provider-verifier_1  |   Given provider in some state
provider-verifier_1  |     request to get non-existent user's phone number
provider-verifier_1  |       with GET /phone/micky
provider-verifier_1  |         returns a response which
provider-verifier_1  | WARN: Skipping set up for provider state 'provider in some state' for consumer 'basic_client' as there is no --provider-states-setup-url specified.
provider-verifier_1  |           has status code 404
provider-verifier_1  | 
provider-verifier_1  | 2 interactions, 0 failures
```


#### Running both parts in CI pipeline

Now that we have both parts of contract testing sorted it would be nice to have that run on each commit. 
This is where Gitlab CI comes handy. Pipeline jobs are described in ```.gitlab-ci.yml```. Before we move on to pipelines we have to say a few words
about [GitLab Runner](https://docs.gitlab.com/runner/) which is  *the open source project that is used to run your jobs and send the results back to GitLab.* 
Jobs can be run locally or using Docker containers. The latter approach is used in our pipeline.
Test infrastructure is containerized and is described in ```docker-compose.yml``` located in the root of the project.
Let's take a quick look. 
```
version: '2'

services:

  basic-flask-app:
    image: registry.gitlab.com/tknino69/basic_flask_app:latest
    ports:
      - 5005:5005

  postgres:
    image: postgres
    ports:
      - 5432:5432
    env_file:
      - test-setup.env
    volumes:
      - db-data:/var/lib/postgresql/data/pgdata

  pactbroker:
    image: dius/pact-broker
    links:
      - postgres
    ports:
      - 80:80
    env_file:
      - test-setup.env

  provider-states:
    image: registry.gitlab.com/tknino69/cdc/provider-states:latest
    build: provider-states
    ports:
      - 5000:5000

  consumer-test:
    image: registry.gitlab.com/tknino69/cdc/consumer-test:latest
    command: ["sh", "-c", "find -name '*.pyc' -delete && pytest $${TEST}"]
    links:
      - pactbroker
    environment:
      - CONSUMER_VERSION=$CI_COMMIT_SHA

  provider-verifier:
    image: registry.gitlab.com/tknino69/cdc/provider-verifier:latest
    build: provider-verifier
    ports:
      - 5001:5000
    links:
      - pactbroker
    depends_on:
      - consumer-test
      - provider-states
    command: ['sh', '-c', 'find -name "*.pyc" -delete
               && CONSUMER_VERSION=`curl --header "PRIVATE-TOKEN:$${API_TOKEN}"
               https://gitlab.com/api/v4/projects/$${BASIC_CLIENT}/repository/commits | jq ".[0] .id" | sed -e "s/\x22//g"`
               && echo $${CONSUMER_VERSION}
               && pact-provider-verifier $${PACT_BROKER}/pacts/provider/$${PROVIDER}/consumer/$${CONSUMER}/version/$${CONSUMER_VERSION}
               --provider-base-url=$${BASE_URL}
               --pact-broker-base-url=$${PACT_BROKER}
               --provider=$${PROVIDER}
               --consumer-version-tag=$${CONSUMER_VERSION}
               --provider-app-version=$${PROVIDER_VERSION} -v
               --publish-verification-results=PUBLISH_VERIFICATION_RESULTS']
    environment:
      - PROVIDER_VERSION=$CI_COMMIT_SHA
      - API_TOKEN=$API_TOKEN
    env_file:
      - test-setup.env

volumes:
  db-data:
```

Here, we have services that are launched in containers as we need them.

Our Provider service:
```
basic-flask-app:
    image: registry.gitlab.com/tknino69/basic_flask_app:latest
    ports:
      - 5005:5005
```

Pact Broker App and its postgres db. Volumes allow us to have persistent storage for pacts and verification results
```
postgres:
    image: postgres
    ports:
      - 5432:5432
    env_file:
      - test-setup.env
    volumes:
      - db-data:/var/lib/postgresql/data/pgdata

  pactbroker:
    image: dius/pact-broker
    links:
      - postgres
    ports:
      - 80:80
    env_file:
      - test-setup.env

```

Provider States App. This one is supposed to set state of the provider. However, in our example it just carries out a dummy function
```
provider-states:
    image: registry.gitlab.com/tknino69/cdc/provider-states:latest
    build: provider-states
    ports:
      - 5000:5000
```

Service that runs Consumer Test. Please note the command that is run in the container ```find -name '*.pyc' -delete && pytest $${TEST}```
```
consumer-test:
    image: registry.gitlab.com/tknino69/cdc/consumer-test:latest
    command: ["sh", "-c", "find -name '*.pyc' -delete && pytest $${TEST}"]
    links:
      - pactbroker
    environment:
      - CONSUMER_VERSION=$CI_COMMIT_SHA
```

Provider Verifier service

```
provider-verifier:
    image: registry.gitlab.com/tknino69/cdc/provider-verifier:latest
    build: provider-verifier
    ports:
      - 5001:5000
    links:
      - pactbroker
    depends_on:
      - consumer-test
      - provider-states
    command: ['sh', '-c', 'find -name "*.pyc" -delete
               && CONSUMER_VERSION=`curl --header "PRIVATE-TOKEN:$${API_TOKEN}"
               https://gitlab.com/api/v4/projects/$${BASIC_CLIENT}/repository/commits | jq ".[0] .id" | sed -e "s/\x22//g"`
               && echo $${CONSUMER_VERSION}
               && pact-provider-verifier $${PACT_BROKER}/pacts/provider/$${PROVIDER}/consumer/$${CONSUMER}/version/$${CONSUMER_VERSION}
               --provider-base-url=$${BASE_URL}
               --pact-broker-base-url=$${PACT_BROKER}
               --provider=$${PROVIDER}
               --consumer-version-tag=$${CONSUMER_VERSION}
               --provider-app-version=$${PROVIDER_VERSION} -v
               --publish-verification-results=PUBLISH_VERIFICATION_RESULTS']
    environment:
      - PROVIDER_VERSION=$CI_COMMIT_SHA
      - API_TOKEN=$API_TOKEN
    env_file:
      - test-setup.env
```


#### Consumer Pipeline
 ```.gitlab-ci.yml``` in the [Consumer project](https://gitlab.com/tknino69/basic_client) root describes jobs that are performed once triggered.

```
image: gitlab/dind:latest

variables:
  TEST: 'tests/docker-compose.app.yml'
  CONSUMER_VERSION: $CI_COMMIT_SHA
  BASIC_APP: '11993024'

services:
   - gitlab/gitlab-runner:latest

before_script:
  - docker login -u $GIT_USER -p $GIT_PASS registry.gitlab.com

stages:
  - clone_test
  - get_broker_up
  - test
  - verify_provider
  - clean_up

clone test:
  tags:
    - cdc
  stage: clone_test
  script:
    - git clone https://$GIT_USER:$GIT_PASS@gitlab.com/tknino69/cdc.git && ls -ali
  artifacts:
    paths:
    - cdc/

broker:
  tags:
    - cdc
  stage: get_broker_up
  script:
    - cd cdc && docker-compose -f docker-compose.yml up -d pactbroker
  dependencies:
    - clone test

test:
  tags:
    - cdc
  stage: test
  script:
    - cd cdc && CONSUMER_VERSION=$CONSUMER_VERSION docker-compose -f docker-compose.yml -f $TEST up consumer-test
  dependencies:
    - clone test

provider verification:
  tags:
    - cdc
  stage: verify_provider
  script:
    - curl -X POST -F token=$CI_JOB_TOKEN -F ref=master https://gitlab.com/api/v4/projects/$BASIC_APP/trigger/pipeline
  when: on_success

clean up:
  tags:
    - cdc
  stage: clean_up
  script:
    - cd cdc && docker-compose stop consumer-test
  dependencies:
    - clone test
```

Essentially, what happens is:
* In ```before_script```, we logging into our gitlab registry using $GIT_USER and $GIT_PASS which we set in
 Settings > CI/CD section
 ![Variables](static/env_variables.PNG)
* Next, we clone test project
* In the next stage we bring up Pact Broker
* Then we perform consumer test
* Next, we are using Gitlab API to trigger provider verification job on the provider side
* And lastly, we perform some clean up




#### Provider pipeline
Provider pipeline configuration stored in ```.gitlab-ci.yml``` in the root of the [Provider project](https://gitlab.com/tknino69/basic_flask_app).

```
image: gitlab/dind:latest

variables:
  TEST: 'tests/docker-compose.app.yml'
  PROVIDER_VERSION: $CI_COMMIT_SHA

services:
  - gitlab/gitlab-runner:latest

stages:
  - clone_test
  - provider_verification
  - clean_up

clone test:
  tags:
    - cdc
  stage: clone_test
  script:
    - git clone https://$GIT_USER:$GIT_PASS@gitlab.com/tknino69/cdc.git
  artifacts:
    paths:
    - cdc/

verify provider:
  tags:
    - cdc
  stage: provider_verification
  before_script:
    - cd cdc
    - docker login -u $GIT_USER -p $GIT_PASS registry.gitlab.com && docker-compose -f docker-compose.yml up -d basic-flask-app
  script:
    - PROVIDER_VERSION=$PROVIDER_VERSION docker-compose -f docker-compose.yml -f $TEST up provider-verifier
  dependencies:
    - clone test

.clean up:
  tags:
    - cdc
  stage: clean_up
  script:
    - cd cdc && docker-compose down --rmi local
```

Similar to Consumer pipeline we have several jobs:
* Clone test project
* Perform Provider verification
* And clean up

#### To summarize:
* We've implemented basic [Provider(Flask app)](https://gitlab.com/tknino69/basic_flask_app/blob/master/app.py) 
and basic [Consumer(simple client)](https://gitlab.com/tknino69/basic_client/blob/master/client.py)
* We've written CDC test in Python
* We've set up containerized test environment
* And finally, we've set up Continuous Integration for consumer driven contract testing, i.e. any commit into consumer project
will trigger CI pipeline(on Consumer side: Clone Test -> Get Pact Broker up -> Consumer Test -> Trigger Provider Verification -> Clean up;
on Provider side: Clone Test -> Verify Provider -> Clean up). And any commit into provider project will trigger provider verification to ensure
that the provider can still fulfill the contract(pact)


                                        
 
 p.s. There are plenty of room for improvement (just to name a few):
 * Implement proper Provider States
 * Implement better housekeeping (i.e. removing containers/images etc)
 * Work out a way to scale for larger projects
 
 debug
 1 test1